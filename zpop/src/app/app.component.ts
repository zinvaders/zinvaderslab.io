import { Component, OnInit } from '@angular/core';
import { trigger, state, style, animate, transition, group } from '@angular/animations';
import { ZombieCard } from './zombieCard';
import spoilerAbominations from '../assets/xenos/spoiler-abomination.json';
import workers from '../assets/xenos/workers.json';
import hunters from '../assets/xenos/hunters.json';
import tanks from '../assets/xenos/tanks.json';
import flingers from '../assets/xenos/flingers.json';
import drillerBlitzers from '../assets/xenos/driller-blitzers.json';
import seekerWorkers from '../assets/xenos/seeker-workers.json';
import xenomoths from '../assets/xenos/xenomoths.json';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less'],
  animations: [
    trigger('flyInOut', [
      state('in', style({
        width: 120,
        transform: 'translateX(0)', opacity: 1
      })),
      transition('in => out', [
        style({ width: 10, transform: 'translateX(50px)', opacity: 0 }),
        group([
          animate('0.3s 0.1s ease', style({
            transform: 'translateX(0)',
            width: 120
          })),
          animate('0.3s ease', style({
            opacity: 1
          }))
        ])
      ]),
      transition('out => in', [
        group([
          animate('0.3s ease', style({
            transform: 'translateX(50px)',
            width: 10
          })),
          animate('0.3s 0.2s ease', style({
            opacity: 0
          }))
        ])
      ])
    ])
  ]

  /*trigger('fadeInCard', [
    state('initial', style({opacity: 1})),
    state('final', style({opacity: 0})),
    transition('initial=>final', animate('600ms')),
    transition('final=>initial', animate('600ms'))
  ])*/
  
})

export class AppComponent implements OnInit {

  spoiler = false;
  worker = false;
  hunter = false;
  tank = false;
  blitzer = false;
  flinger = false;
  seeker = false;
  xenomoth = false;

  enemyChoise = false;

  zombieCards : ZombieCard[];
  zombieOldCards : ZombieCard[] = new Array<ZombieCard>();
  currentCard;// = "./assets/cards/cover.jpg"
  index;

  currentState = 'in';


  setXeno(xeno){
    if(xeno==="spoiler")
      this.spoiler = !this.spoiler;
    else if(xeno==="worker")
      this.worker = !this.worker;
    else if(xeno==="hunter")
      this.hunter = !this.hunter;
    else if(xeno==="tank")
      this.tank = !this.tank;
    else if(xeno==="blitzer")
      this.blitzer = !this.blitzer;
    else if(xeno==="flinger")
      this.flinger = !this.flinger;
    else if(xeno==="seeker")
      this.seeker = !this.seeker;
    else if(xeno==="xenomoth")
      this.xenomoth = !this.xenomoth;

    this.enemyChoise = this.spoiler || this.worker || this.hunter || this.tank || this.blitzer || this.flinger || this.seeker || this.xenomoth;
  }
  
  initGame(){
    //this.printEnemyChoise();
    this.newZombieDeck();
    //this.printZombieDeck();
    this.nextCard();
  }

  newZombieDeck(){
    this.zombieCards = new Array<ZombieCard>();
    this.index = 0;

    let zombies = [];

    if(this.spoiler)
      zombies = zombies.concat(spoilerAbominations);
    if(this.worker)
      zombies = zombies.concat(workers);
    if(this.hunter)
      zombies = zombies.concat(hunters);
    if(this.tank)
      zombies = zombies.concat(tanks);
    if(this.blitzer)
      zombies = zombies.concat(drillerBlitzers);
    if(this.flinger)
      zombies = zombies.concat(flingers);
    if(this.seeker)
      zombies = zombies.concat(seekerWorkers);
    if(this.xenomoth)
      zombies = zombies.concat(xenomoths);

     

    for(var i = 0; i < zombies.length; i++)
      for(var j = 0; j < zombies[i].quantity; j++)
        this.zombieCards.push(zombies[i]);


    //shuffle
    for(let i = this.zombieCards.length-1; i > 0; i--){
      const j = Math.floor(Math.random() * i)
      const temp = this.zombieCards[i]
      this.zombieCards[i] = this.zombieCards[j]
      this.zombieCards[j] = temp
    }
    this.zombieCards.sort(() => Math.random() - 0.5); 
  }

  nextCard(){

    this.currentState = this.currentState === 'in' ? 'out' : 'in';

    //add card in the old deck
    if(this.currentCard)
      this.zombieOldCards.unshift(this.currentCard);

    this.currentCard = this.zombieCards[this.index];
    this.index++;
    
    console.log(this.index + "  /  " + this.zombieCards.length);
    if(this.index >= this.zombieCards.length)
      this.initGame();
  
  }

  printZombieDeck(){
    this.zombieCards.forEach(function (zombieCard) {
      console.log(zombieCard);
    });
  }

  printEnemyChoise(){
    console.log("spoiler: " + this.spoiler);
    console.log("worker: " + this.worker);
    console.log("hunter: " + this.hunter);
    console.log("tank: " + this.tank);
    console.log("blitzer: " + this.blitzer);
    console.log("flinger: " + this.flinger);
    console.log("seeker: " + this.seeker);
    console.log("xenomoth: " + this.xenomoth);
  }
  
  constructor() {}

  ngOnInit() {
    //this.initGame();
  }

}

export class ZombieCard {
    id: number;
    title: string;
    img: string;
    quantity: number;
    blue: number;
    yellow: number;
    orange: number;
    red: number;
  }

  